//
// This file is part of the GNU ARM Eclipse distribution.
// Copyright (c) 2014 Liviu Ionescu.
//

#ifndef BLINKLED_H_
#define BLINKLED_H_

#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"





#define BLINK_GPIOx(_N)                 ((GPIO_TypeDef *)(GPIOA_BASE + (GPIOB_BASE-GPIOA_BASE)*(_N)))
#define BLINK_PIN_MASK(_N)              (1 << (_N))
#define BLINK_RCC_MASKx(_N)             (RCC_AHB1ENR_GPIOAEN << (_N))

// ----------------------------------------------------------------------------

extern void disco_led_init(void);
extern uint8_t disco_led_bar(uint8_t val);

// ----------------------------------------------------------------------------

inline void
disco_led_on(int led);

inline void
disco_led_off(int led);

// ----------------------------------------------------------------------------

static int disco_led_pins[4] = {12, 13, 14, 15};




// ----------------------------------------------------------------------------

#endif // BLINKLED_H_
