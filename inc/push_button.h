/*
 * push_button.h
 *
 *  Created on: 7 lip 2015
 *      Author: grom
 */

#ifndef PUSH_BUTTON_H_
#define PUSH_BUTTON_H_

#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"


#define BUTTON0_PORT_RCC RCC_AHB1ENR_GPIOAEN
#define BUTTON0_PORT GPIOA
#define BUTTON0_PIN GPIO_PIN_0


extern void button_init(void);
extern void button_irqhandler(void);



#endif /* PUSH_BUTTON_H_ */
