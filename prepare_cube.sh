#!/bin/bash

#rm -v stm32cubef4.zip*
#rm -vd STM32Cube_FW_F4*

#wget http://www.st.com/st-web-ui/static/active/en/st_prod_software_internet/resource/technical/software/firmware/stm32cubef4.zip

#unzip stm32cubef4.zip

CUBE_DIR=$(ls -d STM32Cube_FW_F4_V*)
echo $CUBE_DIR

rm -rvdf cube

mkdir -v cube 
mkdir -v cube/inc
mkdir -v cube/stm32f4xx_hal
mkdir -v cube/stm32f4xx_hal/inc
mkdir -v cube/stm32f4xx_hal/src

cp -v $CUBE_DIR/Drivers/CMSIS/Include/* cube/inc
cp -v $CUBE_DIR/Drivers/CMSIS/Device/ST/STM32F4xx/Include/* cube/inc
cp -vr $CUBE_DIR/Drivers/STM32F4xx_HAL_Driver/Inc/* cube/stm32f4xx_hal/inc
cp -vr $CUBE_DIR/Drivers/STM32F4xx_HAL_Driver/Src/* cube/stm32f4xx_hal/src

#rm -v stm32cubef4.zip*
#rm -vd STM32Cube_FW_F4*
