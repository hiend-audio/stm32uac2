/*
 * push_button.c
 *
 *  Created on: 7 lip 2015
 *      Author: grom
 */

#include "../inc/push_button.h"

#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_cortex.h"
#include "../inc/USB/uac2/usbd_audio.h"
#include "../inc/USB/usbd/usb_device.h"

void button_init(){


	RCC->AHB1ENR |= BUTTON0_PORT_RCC;

	GPIO_InitTypeDef GPIO_InitStructure;

	// Configure pin in output push/pull mode
	GPIO_InitStructure.Pin = BUTTON0_PIN;
	GPIO_InitStructure.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;
	GPIO_InitStructure.Pull = GPIO_PULLDOWN;

	HAL_GPIO_Init(BUTTON0_PORT, &GPIO_InitStructure);


	HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI0_IRQn);


}

void button_irqhandler(void)
{

	USBD_AUDIO_ItfTypeDef *fops = hUsbDeviceFS.pUserData;


	static struct {
		uint8_t bInfo;
		uint8_t bAttribute;
		uint16_t wValue;
		uint16_t wIndex;
	} status = {
		.bInfo = 0x00, // Interface interrupt
		.bAttribute = AUDIO_REQ_CUR,
		.wValue = (AUDIO_CS_VOLUME_CONTROL << 8) | 0,
		.wIndex = (AUDIO_FEATURE_UNIT_ID << 8) | 0,
	};

	fops->VolumeCtl(usbd_audio_volume + 0x100);

	USBD_LL_Transmit(&hUsbDeviceFS, 0x82, &status, 6);
	//asm volatile ("nop");
}
