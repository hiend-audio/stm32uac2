/**
 ******************************************************************************
 * @file    usbd_audio.c
 * @author  MCD Application Team
 * @version V2.4.0
 * @date    28-February-2015
 * @brief   This file provides the Audio core functions.
 *
 * @verbatim
 *
 *          ===================================================================
 *                                AUDIO Class  Description
 *          ===================================================================
 *           This driver manages the Audio Class 1.0 following the "USB Device Class Definition for
 *           Audio Devices V1.0 Mar 18, 98".
 *           This driver implements the following aspects of the specification:
 *             - Device descriptor management
 *             - Configuration descriptor management
 *             - Standard AC Interface Descriptor management
 *             - 1 Audio Streaming Interface (with single channel, PCM, Stereo mode)
 *             - 1 Audio Streaming Endpoint
 *             - 1 Audio Terminal Input (1 channel)
 *             - Audio Class-Specific AC Interfaces
 *             - Audio Class-Specific AS Interfaces
 *             - AudioControl Requests: only SET_CUR and GET_CUR requests are supported (for Mute)
 *             - Audio Feature Unit (limited to Mute control)
 *             - Audio Synchronization type: Asynchronous
 *             - Single fixed audio sampling rate (configurable in usbd_conf.h file)
 *          The current audio class version supports the following audio features:
 *             - Pulse Coded Modulation (PCM) format
 *             - sampling rate: 48KHz.
 *             - Bit resolution: 16
 *             - Number of channels: 2
 *             - No volume control
 *             - Mute/Unmute capability
 *             - Asynchronous Endpoints
 *
 * @note     In HS mode and when the DMA is used, all variables and data structures
 *           dealing with the DMA during the transaction process should be 32-bit aligned.
 *
 *
 *  @endverbatim
 *
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
 *
 * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        http://www.st.com/software_license_agreement_liberty_v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "../../../inc/USB/uac2/usbd_audio.h"

#include "../../../inc/disco_leds.h"
#include "../../../inc/USB/usbd/usbd_ctlreq.h"
#include "../../../inc/USB/usbd/usbd_desc.h"

/** @addtogroup STM32_USB_DEVICE_LIBRARY
 * @{
 */

/** @defgroup USBD_AUDIO 
 * @brief usbd core module
 * @{
 */

/** @defgroup USBD_AUDIO_Private_TypesDefinitions
 * @{
 */
/**
 * @}
 */

/** @defgroup USBD_AUDIO_Private_Defines
 * @{
 */

/**
 * @}
 */

/** @defgroup USBD_AUDIO_Private_Macros
 * @{
 */
#define AUDIO_SAMPLE_FREQ(frq)      (uint8_t)(frq), (uint8_t)((frq >> 8)), (uint8_t)((frq >> 16))

#define AUDIO_PACKET_SZE(frq)          (uint8_t)((((frq+999) * 2)/1000) & 0xFF), \
		(uint8_t)(((((frq+999) * 2)/1000) >> 8) & 0xFF)

/**
 * @}
 */

/** @defgroup USBD_AUDIO_Private_FunctionPrototypes
 * @{
 */

static uint8_t USBD_AUDIO_Init(USBD_HandleTypeDef *pdev, uint8_t cfgidx);

static uint8_t USBD_AUDIO_DeInit(USBD_HandleTypeDef *pdev, uint8_t cfgidx);

static uint8_t USBD_AUDIO_Setup(USBD_HandleTypeDef *pdev,
		USBD_SetupReqTypedef *req);

static uint8_t *USBD_AUDIO_GetCfgDesc(uint16_t *length);

static uint8_t *USBD_AUDIO_GetDeviceQualifierDesc(uint16_t *length);

static uint8_t USBD_AUDIO_DataIn(USBD_HandleTypeDef *pdev, uint8_t epnum);

static uint8_t USBD_AUDIO_DataOut(USBD_HandleTypeDef *pdev, uint8_t epnum);

static uint8_t USBD_AUDIO_EP0_RxReady(USBD_HandleTypeDef *pdev);

static uint8_t USBD_AUDIO_EP0_TxReady(USBD_HandleTypeDef *pdev);

static uint8_t USBD_AUDIO_SOF(USBD_HandleTypeDef *pdev);

static uint8_t USBD_AUDIO_IsoINIncomplete(USBD_HandleTypeDef *pdev,
		uint8_t epnum);

static uint8_t USBD_AUDIO_IsoOutIncomplete(USBD_HandleTypeDef *pdev,
		uint8_t epnum);

static void AUDIO_REQ_GetCurMinMax(USBD_HandleTypeDef *pdev,
		USBD_SetupReqTypedef *req);

static void AUDIO_REQ_SetCurMinMax(USBD_HandleTypeDef *pdev,
		USBD_SetupReqTypedef *req);

/**
 * @}
 */

/** @defgroup USBD_AUDIO_Private_Variables
 * @{
 */

USBD_ClassTypeDef USBD_AUDIO = { USBD_AUDIO_Init, USBD_AUDIO_DeInit,
		USBD_AUDIO_Setup, USBD_AUDIO_EP0_TxReady, USBD_AUDIO_EP0_RxReady,
		USBD_AUDIO_DataIn, USBD_AUDIO_DataOut, USBD_AUDIO_SOF,
		USBD_AUDIO_IsoINIncomplete, USBD_AUDIO_IsoOutIncomplete,
		USBD_AUDIO_GetCfgDesc, USBD_AUDIO_GetCfgDesc, USBD_AUDIO_GetCfgDesc,
		USBD_AUDIO_GetDeviceQualifierDesc, };

/* USB AUDIO device Configuration Descriptor */
__ALIGN_BEGIN static uint8_t USBD_AUDIO_CfgDesc[USB_AUDIO_CONFIG_DESC_SIZ] __ALIGN_END
= {
		/* Configuration 1 */
		0x09, /* bLength */
		USB_DESC_TYPE_CONFIGURATION, /* bDescriptorType */
		LOBYTE(USB_AUDIO_CONFIG_DESC_SIZ), /* wTotalLength  148 bytes*/
		HIBYTE(USB_AUDIO_CONFIG_DESC_SIZ), 0x02, /* bNumInterfaces */
		0x01, /* bConfigurationValue */
		0x00, /* iConfiguration */
		0xC0, /* bmAttributes  BUS Powred*/
		0x32, /* bMaxPower = 100 mA*/
		/* 09 byte*/

		/* Interface Association descriptor */
		AUDIO_INTERFACE_ASSOC_DESC_SIZE, /* bLength */
		USB_DESC_TYPE_INTERFACE_ASSOC, /* bDescriptorType */
		0x00, /* bFirstInterface */
		0x02, /* bInterfaceCount */
		AUDIO_FUNCTION_CLASS, /* bFunctionClass */
		AUDIO_FUNCTION_SUBCLASS_UNDEF, /* bFunctionSubclass */
		AUDIO_FUNCTION_AF_VERSION_02_00, /* bFuctionProtocol */
		0x00, /* iFunction */
		/* 08 byte */

		/* USB Speaker Standard interface descriptor */
		AUDIO_INTERFACE_DESC_SIZE, /* bLength */
		USB_DESC_TYPE_INTERFACE, /* bDescriptorType */
		0x00, /* bInterfaceNumber */
		0x00, /* bAlternateSetting */
		0x01, /* bNumEndpoints */
		USB_DEVICE_CLASS_AUDIO, /* bInterfaceClass */
		AUDIO_SUBCLASS_AUDIOCONTROL, /* bInterfaceSubClass */
		AUDIO_PROTOCOL_IP_VERSION_02_00, /* bInterfaceProtocol */
		0x00, /* iInterface */
		/* 09 byte*/

		/* USB Speaker Class-specific AC Interface Descriptor */
		AUDIO_INTERFACE_DESC_SIZE, /* bLength */
		AUDIO_INTERFACE_DESCRIPTOR_TYPE, /* bDescriptorType */
		AUDIO_CONTROL_HEADER, /* bDescriptorSubtype */
		0x00, /* 2.00 *//* bcdADC */
		0x02,
		AUDIO_CAT_FUNCTION_SUBCLASS_UNDEF, /* bCategory */
		60, /* wTotalLength = 60 */
		0x00, 0x00, /* bmControls */
		/* 09 byte*/

		/* USB Speaker Clock Source Descriptor */
		AUDIO_CLOCK_SOURCE_DESC_SIZE, /* bLength */
		AUDIO_INTERFACE_DESCRIPTOR_TYPE, /* bDescriptorType */
		AUDIO_CONTROL_CLOCK_SOURCE, /* bDescriptorSubtype */
		0x04, /* bClockID */
		0x01, /* bmAttributes = internal fixed clock, not synced to SOF */
		0x05, /* bmControls = read-only clock validity, read-only sampling freq */
		0x01, /* bAssocTerminal */
		0x00, /* iClockSource */
		/* 08 byte */

		/* USB Speaker Input Terminal Descriptor */
		AUDIO_INPUT_TERMINAL_DESC_SIZE, /* bLength */
		AUDIO_INTERFACE_DESCRIPTOR_TYPE, /* bDescriptorType */
		AUDIO_CONTROL_INPUT_TERMINAL, /* bDescriptorSubtype */
		0x01, /* bTerminalID */
		0x01, /* wTerminalType AUDIO_TERMINAL_USB_STREAMING   0x0101 */
		0x01, 0x00, /* bAssocTerminal */
		0x04, /* bCSourceID */
		0x01, /* bNrChannels */
		0x00, /* bmChannelConfig 0x80000000  Mono, raw data */
		0x00, 0x00, 0x80, 0x00, /* iChannelNames */
		0x00, /* bmControls */
		0x00, 0x00, /* iTerminal */
		/* 17 byte*/

		/* USB Speaker Audio Feature Unit Descriptor */
		14, /* bLength */
		AUDIO_INTERFACE_DESCRIPTOR_TYPE, /* bDescriptorType */
		AUDIO_CONTROL_FEATURE_UNIT, /* bDescriptorSubtype */
		AUDIO_OUT_STREAMING_CTRL, /* bUnitID */
		0x01, /* bSourceID */
		0x0 | (0x0 << 2), /* bmaControls(0) = mute r/w, volume r/w */
		0,
		0,
		0,
		0x3 | (0x3 << 2),
		0,
		0,
		0,
		0x00, /* iFeature */
		/* 14 byte*/

		/*USB Speaker Output Terminal Descriptor */
		AUDIO_OUTPUT_TERMINAL_DESC_SIZE, /* bLength */
		AUDIO_INTERFACE_DESCRIPTOR_TYPE, /* bDescriptorType */
		AUDIO_CONTROL_OUTPUT_TERMINAL, /* bDescriptorSubtype */
		0x03, /* bTerminalID */
		0x01, /* wTerminalType  0x0302 Headphones*/
		0x03,
		0x00, /* bAssocTerminal */
		0x02, /* bSourceID */
		0x04, /* bCSourceID */
		0x00, /* bmControls */
		0x00,
		0x00, /* iTerminal */
		/* 12 byte*/

		//---//
		/*Standard AC Interrupt Endpoint Descriptor*/
		0x07, /* bLength */
		USB_DESC_TYPE_ENDPOINT, /* bDescriptorType */
		AUDIO_IN_EP | 2, /* bEndpointAddress */
		USBD_EP_TYPE_INTR, /* bmAttributes */
		6, /* wMaxPacketSize */
		0x00, 0xC8, /* bInterval */
		/* 07 byte*/

		/* USB Speaker Standard AS Interface Descriptor - Audio Streaming Zero Bandwith */
		/* Interface 1, Alternate Setting 0                                             */
		AUDIO_INTERFACE_DESC_SIZE, /* bLength */
		USB_DESC_TYPE_INTERFACE, /* bDescriptorType */
		0x01, /* bInterfaceNumber */
		0x00, /* bAlternateSetting */
		0x00, /* bNumEndpoints */
		USB_DEVICE_CLASS_AUDIO, /* bInterfaceClass */
		AUDIO_SUBCLASS_AUDIOSTREAMING, /* bInterfaceSubClass */
		AUDIO_PROTOCOL_IP_VERSION_02_00, /* bInterfaceProtocol */
		0x00, /* iInterface */
		/* 09 byte*/

		/* USB Speaker Standard AS Interface Descriptor - Audio Streaming Operational */
		/* Interface 1, Alternate Setting 1                                           */
		AUDIO_INTERFACE_DESC_SIZE, /* bLength */
		USB_DESC_TYPE_INTERFACE, /* bDescriptorType */
		0x01, /* bInterfaceNumber */
		0x01, /* bAlternateSetting */
		0x01, /* bNumEndpoints */
		USB_DEVICE_CLASS_AUDIO, /* bInterfaceClass */
		AUDIO_SUBCLASS_AUDIOSTREAMING, /* bInterfaceSubClass */
		AUDIO_PROTOCOL_IP_VERSION_02_00, /* bInterfaceProtocol */
		0x00, /* iInterface */
		/* 09 byte*/

		/* USB Speaker Audio Streaming Interface Descriptor */
		AUDIO_STREAMING_INTERFACE_DESC_SIZE, /* bLength */
		AUDIO_INTERFACE_DESCRIPTOR_TYPE, /* bDescriptorType */
		AUDIO_STREAMING_GENERAL, /* bDescriptorSubtype */
		0x01, /* bTerminalLink */
		0x00, /* bmControls */
		AUDIO_FORMAT_TYPE_I, /* bFormatType */
		0x01, /* bmFormats, D0 = PCM */
		0x00, 0x00, 0x00, 0x01, /* bNrChannels */
		0x80, /* bmChannelConfig = raw data */
		0x00, 0x00, 0x00, 0x00, /* iChannelNames */
		/* 16 byte*/

		/* USB Speaker Audio Type I Format Interface Descriptor */
		0x06, /* bLength */
		AUDIO_INTERFACE_DESCRIPTOR_TYPE, /* bDescriptorType */
		AUDIO_STREAMING_FORMAT_TYPE, /* bDescriptorSubtype */
		AUDIO_FORMAT_TYPE_I, /* bFormatType */
		0x02, /* bSubslotSize :  2 Bytes per slot (16bits) */
		16, /* bBitResolution (16-bits per sample) */
		/* 06 byte*/

		/* Endpoint 1 - Standard Descriptor */
		7, /* bLength */
		USB_DESC_TYPE_ENDPOINT, /* bDescriptorType */
		AUDIO_OUT_EP, /* bEndpointAddress 1 out endpoint*/
		USBD_EP_TYPE_ISOC | (2<<4) | (3<<2), /* bmAttributes */
		AUDIO_PACKET_SZE(USBD_AUDIO_FREQ), /* wMaxPacketSize in Bytes (Freq(Samples)*2(Stereo)*2(HalfWord)) */
		0x01, /* bInterval */

		/* 07 byte*/

		/* Endpoint - Audio Streaming Descriptor*/
		AUDIO_STREAMING_ENDPOINT_DESC_SIZE, /* bLength */
		AUDIO_ENDPOINT_DESCRIPTOR_TYPE, /* bDescriptorType */
		AUDIO_ENDPOINT_GENERAL, /* bDescriptor */
		0x00, /* bmAttributes */
		0x00, /* bmControls */
		0x01, /* bLockDelayUnits */
		0x40, /* wLockDelay */
		0x00,
		/* 08 byte*/

};

/* USB Standard Device Descriptor */
__ALIGN_BEGIN static uint8_t USBD_AUDIO_DeviceQualifierDesc[USB_LEN_DEV_QUALIFIER_DESC] __ALIGN_END
= {
		USB_LEN_DEV_QUALIFIER_DESC,
		USB_DESC_TYPE_DEVICE_QUALIFIER, 0x00, 0x02, 0xef, 0x02, 0x01,
		USB_MAX_EP0_SIZE, 0x01, 0x00, };

/**
 * @}
 */

int16_t usbd_audio_volume = 0;

/** @defgroup USBD_AUDIO_Private_Functions
 * @{
 */

/**
 * @brief  USBD_AUDIO_Init
 *         Initialize the AUDIO interface
 * @param  pdev: device instance
 * @param  cfgidx: Configuration index
 * @retval status
 */
static uint8_t USBD_AUDIO_Init(USBD_HandleTypeDef *pdev, uint8_t cfgidx) {
	USBD_AUDIO_HandleTypeDef *haudio;

	/* Open EP OUT */
	USBD_LL_OpenEP(pdev,
			AUDIO_OUT_EP,
			USBD_EP_TYPE_ISOC,
			AUDIO_OUT_PACKET);

	/* Open EP IN Interrupt */
	USBD_LL_OpenEP(pdev, 0x82,
			USBD_EP_TYPE_INTR, 6);

	/* Allocate Audio structure */
	pdev->pClassData = USBD_malloc(sizeof(USBD_AUDIO_HandleTypeDef));

	if (pdev->pClassData == NULL) {
		return USBD_FAIL;
	} else {
		haudio = (USBD_AUDIO_HandleTypeDef*) pdev->pClassData;
		haudio->alt_setting = 0;
		haudio->offset = AUDIO_OFFSET_UNKNOWN;
		haudio->wr_ptr = 0;
		haudio->rd_ptr = 0;
		haudio->rd_enable = 0;

		/* Initialize the Audio output Hardware layer */
		if (((USBD_AUDIO_ItfTypeDef *) pdev->pUserData)->Init(USBD_AUDIO_FREQ,
				AUDIO_DEFAULT_VOLUME, 0) != USBD_OK) {
			return USBD_FAIL;
		}

		/* Prepare Out endpoint to receive 1st packet */
		USBD_LL_PrepareReceive(pdev,
				AUDIO_OUT_EP, haudio->buffer,
				AUDIO_OUT_PACKET);
	}
	return USBD_OK;
}

/**
 * @brief  USBD_AUDIO_Init
 *         DeInitialize the AUDIO layer
 * @param  pdev: device instance
 * @param  cfgidx: Configuration index
 * @retval status
 */
static uint8_t USBD_AUDIO_DeInit(USBD_HandleTypeDef *pdev, uint8_t cfgidx) {

	/* Open EP OUT */
	USBD_LL_CloseEP(pdev,
			AUDIO_OUT_EP);

	/* DeInit  physical Interface components */
	if (pdev->pClassData != NULL) {
		((USBD_AUDIO_ItfTypeDef *) pdev->pUserData)->DeInit(0);
		USBD_free(pdev->pClassData);
		pdev->pClassData = NULL;
	}

	return USBD_OK;
}


typedef int (*entity_control_cb)(USBD_HandleTypeDef *pdev, USBD_AUDIO_ControlTypeDef *ctl);

struct AUDIO_Entity_control {
	uint8_t entityID;
	entity_control_cb getCUR;
	entity_control_cb setCUR;
	entity_control_cb getRANGE;
	entity_control_cb setRANGE;
	entity_control_cb getMEM;
	entity_control_cb setMEM;
};

__packed struct AUDIO_Layout1_CUR_PBlock {
	uint8_t bCur;
};

__packed struct AUDIO_Layout1_RANGE_PBlock {
	uint16_t wNumSubRanges;
	struct {
		uint8_t bMIN;
		uint8_t bMAX;
		uint8_t bRES;
	} r[];
};

__packed struct AUDIO_Layout2_CUR_PBlock {
	uint16_t wCur;
};

__packed struct AUDIO_Layout2_RANGE_PBlock {
	uint16_t wNumSubRanges;
	__packed struct {
		uint16_t wMIN;
		uint16_t wMAX;
		uint16_t wRES;
	} r[];
};

__packed struct AUDIO_Layout3_CUR_PBlock {
	uint32_t dCur;
};

__packed struct AUDIO_Layout3_RANGE_PBlock {
	uint16_t wNumSubRanges;
	__packed struct {
		uint32_t dMIN;
		uint32_t dMAX;
		uint32_t dRES;
	} r[];
};

__packed union AUDIO_Control_PBlock {
	struct AUDIO_Layout1_CUR_PBlock lay1_cur;
	struct AUDIO_Layout2_CUR_PBlock lay2_cur;
	struct AUDIO_Layout3_CUR_PBlock lay3_cur;
	struct AUDIO_Layout1_RANGE_PBlock lay1_range;
	struct AUDIO_Layout2_RANGE_PBlock lay2_range;
	struct AUDIO_Layout3_RANGE_PBlock lay3_range;
};

static int featureUnit_getCUR(USBD_HandleTypeDef *pdev,
		USBD_AUDIO_ControlTypeDef *ctl) {
	union AUDIO_Control_PBlock *p = ctl->data;
	uint16_t len;

	switch (ctl->cs) {
	case AUDIO_CS_MUTE_CONTROL:
		/* Send the current mute state */
		p->lay1_cur.bCur = 0;
		len = 1;
		break;
	case AUDIO_CS_VOLUME_CONTROL:
		p->lay2_cur.wCur = usbd_audio_volume;
		len = 2;
		break;
	default:
		return -1;
	}

	if(ctl->len>len)
		ctl->len = len;

	return ctl->len;
}

static int featureUnit_setCUR(USBD_HandleTypeDef *pdev,
		USBD_AUDIO_ControlTypeDef *ctl) {
	union AUDIO_Control_PBlock *p = ctl->data;
	USBD_AUDIO_ItfTypeDef *fops = pdev->pUserData;

	switch (ctl->cs) {
	case AUDIO_CS_MUTE_CONTROL:
		//p->lay1_cur.bCur = 0;
		break;
	case AUDIO_CS_VOLUME_CONTROL:
		usbd_audio_volume = p->lay2_cur.wCur;
		fops->VolumeCtl(usbd_audio_volume);
		break;
	default:
		return -1;
	}

	return 0;
}

static int featureUnit_getRANGE(USBD_HandleTypeDef *pdev,
		USBD_AUDIO_ControlTypeDef *ctl) {
	union AUDIO_Control_PBlock *p = ctl->data;
	uint16_t len;

	switch (ctl->cs) {
	case AUDIO_CS_VOLUME_CONTROL:
		p->lay2_range.wNumSubRanges = 1;
		p->lay2_range.r[0].wMIN = (int16_t)(-50*256);
		p->lay2_range.r[0].wMAX = 0;
		p->lay2_range.r[0].wRES = (int16_t)(1*256);
		len = 8;
		break;
	default:
		return -1;
	}

	if(ctl->len>len)
		ctl->len = len;

	return ctl->len;
}

static int clockSource_getCUR(USBD_HandleTypeDef *pdev,
		USBD_AUDIO_ControlTypeDef *ctl) {
	union AUDIO_Control_PBlock *p = ctl->data;
	uint16_t len = ctl->len;

	switch (ctl->cs) {
	case AUDIO_CS_SAM_FREQ_CTL:
		p->lay3_cur.dCur = 44100;
		len = 4;
		break;
	case AUDIO_CS_CLOCK_VALID_CTL:
		p->lay1_cur.bCur = 1; // clock is always valid
		len = 1;
		break;
	default:
		return -1;
	}

	if(ctl->len>len)
		ctl->len = len;

	return ctl->len;
}

static int clockSource_getRANGE(USBD_HandleTypeDef *pdev,
		USBD_AUDIO_ControlTypeDef *ctl) {
	union AUDIO_Control_PBlock *p = ctl->data;
	uint16_t len;

	switch (ctl->cs) {
	case AUDIO_CS_SAM_FREQ_CTL:
		p->lay3_range.wNumSubRanges = 1;
		p->lay3_range.r[0].dMIN = 44100;
		p->lay3_range.r[0].dMAX = 44100;
		p->lay3_range.r[0].dRES = 0;
		len = 14;
		break;
	default:
		return -1;
	}

	if(ctl->len>len)
		ctl->len = len;

	return ctl->len;
}

#define ARRAY_SIZE(x) (sizeof(x)/sizeof(*(x)))

static const struct AUDIO_Entity_control entity_controls[] = {
	{
		.entityID = AUDIO_FEATURE_UNIT_ID,
		.getCUR = &featureUnit_getCUR,
		.setCUR = &featureUnit_setCUR,
		.getRANGE = &featureUnit_getRANGE,
	}, {
		.entityID = AUDIO_CLOCK_SOURCE_ID,
		.getCUR = &clockSource_getCUR,
		.getRANGE = &clockSource_getRANGE,
	}
};

static inline const struct AUDIO_Entity_control *findEntity(uint8_t entityID) {
	int i;
	for(i=0; i<ARRAY_SIZE(entity_controls); i++) {
		if(entity_controls[i].entityID == entityID)
			return &entity_controls[i];
	}

	return NULL;
}

static entity_control_cb findEntityCB(uint8_t entityID, uint8_t cmd, uint8_t get) {
	int i;
	const struct AUDIO_Entity_control *entity = NULL;;
	entity_control_cb res;

	for(i=0; i<ARRAY_SIZE(entity_controls); i++) {
		if(entity_controls[i].entityID == entityID) {
			entity = &entity_controls[i];
			break;
		}
	}

	if(!entity)
		return NULL;

	res = NULL;

	if(get) {
		switch(cmd) {
		case AUDIO_REQ_CUR:
			res = entity->getCUR;
			break;
		case AUDIO_REQ_RANGE:
			res = entity->getRANGE;
			break;
		case AUDIO_REQ_MEM:
			res = entity->getMEM;
			break;
		}
	} else {
		switch(cmd) {
		case AUDIO_REQ_CUR:
			res = entity->setCUR;
			break;
		case AUDIO_REQ_RANGE:
			res = entity->setRANGE;
			break;
		case AUDIO_REQ_MEM:
			res = entity->setMEM;
			break;
		}
	}

	return res;
}

static uint8_t AUDIO_Class_Request(USBD_HandleTypeDef *pdev,
		USBD_SetupReqTypedef *req) {
	USBD_AUDIO_HandleTypeDef *haudio;
	USBD_AUDIO_ControlTypeDef *ctl;
	entity_control_cb entityCB;

	haudio = (USBD_AUDIO_HandleTypeDef*) pdev->pClassData;
	ctl = &haudio->control;



	ctl->cmd = req->bRequest; /* Set the request value */
	ctl->len = req->wLength; /* Set the request data length */
	ctl->unit = HIBYTE(req->wIndex); /* Set the request target unit */
	ctl->cs = HIBYTE(req->wValue);
	ctl->cn = LOBYTE(req->wValue);

	entityCB = findEntityCB(ctl->unit, ctl->cmd, req->bmRequest & USB_REQ_TYPE_GET);

	if(!entityCB)
		return USBD_FAIL;

	if(req->bmRequest & USB_REQ_TYPE_GET) {
		int res;
		res = entityCB(pdev, ctl);
		if(res<0)
			return USBD_FAIL;

		if(res)
			USBD_CtlSendData(pdev, ctl->data, res);
	} else {
		if(req->wLength) {
			/* Prepare the reception of the buffer over EP0 */
			USBD_CtlPrepareRx(pdev, ctl->data, req->wLength);
		} else {
			if(entityCB(pdev, ctl)<0)
				return USBD_FAIL;
		}
	}

	return USBD_OK;
}

/*
 * @brief  USBD_AUDIO_Setup
 *         Handle the AUDIO specific requests
 * @param  pdev: instance
 * @param  req: usb requests
 * @retval status
 */
static uint8_t USBD_AUDIO_Setup(USBD_HandleTypeDef *pdev,
		USBD_SetupReqTypedef *req) {
	USBD_AUDIO_HandleTypeDef *haudio;
	uint16_t len;
	uint8_t *pbuf;
	uint8_t ret = USBD_OK;
	haudio = (USBD_AUDIO_HandleTypeDef*) pdev->pClassData;

	switch (req->bmRequest & USB_REQ_TYPE_MASK) {
	case USB_REQ_TYPE_CLASS:
		if (AUDIO_Class_Request(pdev, req) != USBD_OK) {
			USBD_CtlError(pdev, req);
			ret = USBD_FAIL;
		}
		break;

	case USB_REQ_TYPE_STANDARD:
		switch (req->bRequest) {
		case USB_REQ_GET_DESCRIPTOR:
			break;

		case USB_REQ_GET_INTERFACE:
			USBD_CtlSendData(pdev, (uint8_t *) haudio->alt_setting, 1);
			break;

		case USB_REQ_SET_INTERFACE:
			if ((uint8_t) (req->wValue) <= USBD_MAX_NUM_INTERFACES) {
				haudio->alt_setting = (uint8_t) (req->wValue);
			} else {
				/* Call the error management function (command will be nacked */
				USBD_CtlError(pdev, req);
			}
			break;

		default:
			//USBD_CtlError(pdev, req);
			//ret = USBD_FAIL;
			break;
		}
		break;
		case USB_REQ_TYPE_VENDOR:
			// led
			if (req->bRequest == 0) {

			} else {
				USBD_CtlSendData(pdev, "Hello, world", 12);
			}
			break;
	}
	return ret;
}

/**
 * @brief  USBD_AUDIO_GetCfgDesc
 *         return configuration descriptor
 * @param  speed : current device speed
 * @param  length : pointer data length
 * @retval pointer to descriptor buffer
 */
static uint8_t *USBD_AUDIO_GetCfgDesc(uint16_t *length) {
	*length = sizeof(USBD_AUDIO_CfgDesc);
	return USBD_AUDIO_CfgDesc;
}

/**
 * @brief  USBD_AUDIO_DataIn
 *         handle data IN Stage
 * @param  pdev: device instance
 * @param  epnum: endpoint index
 * @retval status
 */
static uint8_t USBD_AUDIO_DataIn(USBD_HandleTypeDef *pdev, uint8_t epnum) {

	/* Only OUT data are processed */
	return USBD_OK;
}

/**
 * @brief  USBD_AUDIO_EP0_RxReady
 *         handle EP0 Rx Ready event
 * @param  pdev: device instance
 * @retval status
 */
static uint8_t USBD_AUDIO_EP0_RxReady(USBD_HandleTypeDef *pdev) {
	USBD_AUDIO_HandleTypeDef *haudio;
	USBD_AUDIO_ControlTypeDef *ctl;
	entity_control_cb entityCB;

	haudio = (USBD_AUDIO_HandleTypeDef*) pdev->pClassData;
	ctl = &haudio->control;


	entityCB = findEntityCB(ctl->unit, ctl->cmd, 0);

	if(!entityCB)
		return USBD_FAIL;

	if(entityCB(pdev, ctl)<0)
		return USBD_FAIL;

	return USBD_OK;
}
/**
 * @brief  USBD_AUDIO_EP0_TxReady
 *         handle EP0 TRx Ready event
 * @param  pdev: device instance
 * @retval status
 */
static uint8_t USBD_AUDIO_EP0_TxReady(USBD_HandleTypeDef *pdev) {
	/* Only OUT control data are processed */
	return USBD_OK;
}
/**
 * @brief  USBD_AUDIO_SOF
 *         handle SOF event
 * @param  pdev: device instance
 * @retval status
 */
static uint8_t USBD_AUDIO_SOF(USBD_HandleTypeDef *pdev) {
	return USBD_OK;
}

/**
 * @brief  USBD_AUDIO_SOF
 *         handle SOF event
 * @param  pdev: device instance
 * @retval status
 */
void USBD_AUDIO_Sync(USBD_HandleTypeDef *pdev, AUDIO_OffsetTypeDef offset) {
	int8_t shift = 0;
	USBD_AUDIO_HandleTypeDef *haudio;
	haudio = (USBD_AUDIO_HandleTypeDef*) pdev->pClassData;

	haudio->offset = offset;

	if (haudio->rd_enable == 1) {
		haudio->rd_ptr += AUDIO_TOTAL_BUF_SIZE / 2;

		if (haudio->rd_ptr == AUDIO_TOTAL_BUF_SIZE) {
			/* roll back */
			haudio->rd_ptr = 0;
		}
	}

	if (haudio->rd_ptr > haudio->wr_ptr) {
		if ((haudio->rd_ptr - haudio->wr_ptr) < AUDIO_OUT_PACKET) {
			shift = -4;
		} else if ((haudio->rd_ptr - haudio->wr_ptr)
				> (AUDIO_TOTAL_BUF_SIZE - AUDIO_OUT_PACKET )) {
			shift = 4;
		}

	} else {
		if ((haudio->wr_ptr - haudio->rd_ptr) < AUDIO_OUT_PACKET) {
			shift = 4;
		} else if ((haudio->wr_ptr - haudio->rd_ptr)
				> (AUDIO_TOTAL_BUF_SIZE - AUDIO_OUT_PACKET )) {
			shift = -4;
		}
	}

	if (haudio->offset == AUDIO_OFFSET_FULL) {
		((USBD_AUDIO_ItfTypeDef *) pdev->pUserData)->AudioCmd(&haudio->buffer[0],
				AUDIO_TOTAL_BUF_SIZE / 2 - shift, AUDIO_CMD_PLAY);
		haudio->offset = AUDIO_OFFSET_NONE;
	} else if (haudio->offset == AUDIO_OFFSET_HALF) {

		((USBD_AUDIO_ItfTypeDef *) pdev->pUserData)->AudioCmd(
				&haudio->buffer[AUDIO_TOTAL_BUF_SIZE / 2],
				AUDIO_TOTAL_BUF_SIZE / 2 - shift, AUDIO_CMD_PLAY);
		haudio->offset = AUDIO_OFFSET_NONE;

	}
}
/**
 * @brief  USBD_AUDIO_IsoINIncomplete
 *         handle data ISO IN Incomplete event
 * @param  pdev: device instance
 * @param  epnum: endpoint index
 * @retval status
 */
static uint8_t USBD_AUDIO_IsoINIncomplete(USBD_HandleTypeDef *pdev,
		uint8_t epnum) {

	return USBD_OK;
}
/**
 * @brief  USBD_AUDIO_IsoOutIncomplete
 *         handle data ISO OUT Incomplete event
 * @param  pdev: device instance
 * @param  epnum: endpoint index
 * @retval status
 */
static uint8_t USBD_AUDIO_IsoOutIncomplete(USBD_HandleTypeDef *pdev,
		uint8_t epnum) {

	return USBD_OK;
}
/**
 * @brief  USBD_AUDIO_DataOut
 *         handle data OUT Stage
 * @param  pdev: device instance
 * @param  epnum: endpoint index
 * @retval status
 */
static uint8_t USBD_AUDIO_DataOut(USBD_HandleTypeDef *pdev, uint8_t epnum) {
	USBD_AUDIO_HandleTypeDef *haudio;
	haudio = (USBD_AUDIO_HandleTypeDef*) pdev->pClassData;

	if (epnum == AUDIO_OUT_EP) {
		/* Increment the Buffer pointer or roll it back when all buffers are full */

		haudio->wr_ptr += AUDIO_OUT_PACKET;
		disco_led_bar(((haudio->buffer[1] | haudio->buffer[0]<<8 )/6898));

		if (haudio->wr_ptr == AUDIO_TOTAL_BUF_SIZE) {/* All buffers are full: roll back */
			haudio->wr_ptr = 0;

			if (haudio->offset == AUDIO_OFFSET_UNKNOWN) {
				((USBD_AUDIO_ItfTypeDef *) pdev->pUserData)->AudioCmd(
						&haudio->buffer[0],
						AUDIO_TOTAL_BUF_SIZE / 2, AUDIO_CMD_START);
				haudio->offset = AUDIO_OFFSET_NONE;
			}
		}

		if (haudio->rd_enable == 0) {
			if (haudio->wr_ptr == (AUDIO_TOTAL_BUF_SIZE / 2)) {
				haudio->rd_enable = 1;
			}
		}

		/* Prepare Out endpoint to receive next audio packet */
		USBD_LL_PrepareReceive(pdev,
				AUDIO_OUT_EP, &haudio->buffer[haudio->wr_ptr],
				AUDIO_OUT_PACKET);

	}

	return USBD_OK;
}


/**
 * @brief  DeviceQualifierDescriptor
 *         return Device Qualifier descriptor
 * @param  length : pointer data length
 * @retval pointer to descriptor buffer
 */
static uint8_t *USBD_AUDIO_GetDeviceQualifierDesc(uint16_t *length) {
	*length = sizeof(USBD_AUDIO_DeviceQualifierDesc);
	return USBD_AUDIO_DeviceQualifierDesc;
}

/**
 * @brief  USBD_AUDIO_RegisterInterface
 * @param  fops: Audio interface callback
 * @retval status
 */
uint8_t USBD_AUDIO_RegisterInterface(USBD_HandleTypeDef *pdev,
		USBD_AUDIO_ItfTypeDef *fops) {
	if (fops != NULL) {
		pdev->pUserData = fops;
	}
	return 0;
}
/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
