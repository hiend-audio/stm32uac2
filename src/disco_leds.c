//
// This file is part of the GNU ARM Eclipse distribution.
// Copyright (c) 2014 Liviu Ionescu.
//

#include "../inc/disco_leds.h"

// ----------------------------------------------------------------------------

void disco_led_init()
{

	// Enable GPIO Peripheral clock
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIOBEN
	        | RCC_AHB1ENR_GPIOCEN | RCC_AHB1ENR_GPIODEN;

	GPIO_InitTypeDef GPIO_InitStructure;

	// Configure pin in output push/pull mode
	GPIO_InitStructure.Pin = GPIO_PIN_3 | GPIO_PIN_5 | GPIO_PIN_7;
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	HAL_GPIO_Init (GPIOB, &GPIO_InitStructure);

	GPIO_InitStructure.Pin = GPIO_PIN_0 | GPIO_PIN_2 | GPIO_PIN_4
	        | GPIO_PIN_6;
	HAL_GPIO_Init (GPIOD, &GPIO_InitStructure);

	GPIO_InitStructure.Pin = GPIO_PIN_7 | GPIO_PIN_9 | GPIO_PIN_11;
	HAL_GPIO_Init (GPIOC, &GPIO_InitStructure);

	GPIO_InitStructure.Pin = GPIO_PIN_15;
		HAL_GPIO_Init (GPIOA, &GPIO_InitStructure);

	disco_led_bar (0);
}

uint8_t disco_led_bar(uint8_t val)
{

	if(val>9) return 1;

	uint8_t i;

	struct pin {
		GPIO_TypeDef* port;
		uint16_t pin;
	} LED[9] =
		{
			{ GPIOB, GPIO_PIN_7 },
			{ GPIOB, GPIO_PIN_5 },
			{ GPIOB, GPIO_PIN_3 },
			{ GPIOD, GPIO_PIN_6 },
			{ GPIOD, GPIO_PIN_4 },
			{ GPIOD, GPIO_PIN_2 },
			{ GPIOD, GPIO_PIN_0 },
			{ GPIOC, GPIO_PIN_11 },
			{ GPIOA, GPIO_PIN_15 },


		};

	for (i = 0; i < 9; i++) {
		if (i < val) {
			HAL_GPIO_WritePin (LED[i].port, LED[i].pin,
			                   GPIO_PIN_RESET);
		} else {
			HAL_GPIO_WritePin (LED[i].port, LED[i].pin,
			                   GPIO_PIN_SET);
		}
	}

	return 0;

}

// ----------------------------------------------------------------------------
